import 'package:firebase_auth/firebase_auth.dart';
import 'package:redux/redux.dart';

import '../authentication_actions.dart';
import '../user/user.dart';

void checkAuthenticatedMiddleware<T>(Store<T> store,
    CheckAuthenticatedAction action, NextDispatcher next) async {
  try {
    FirebaseUser user = await FirebaseAuth.instance.currentUser();

    if (user != null) {
      var token = await user.getIdToken(refresh: true);
      if (user.isEmailVerified) {
        store.dispatch(SignedInAction(
          user: user.toUser().copyWith(
                claims: token.claims,
              ),
        ));
      } else {
        store.dispatch(SignOutAction());
      }
    } else
      store.dispatch(SignedOutAction());
  } catch (e) {
    throw e;
  }
}
