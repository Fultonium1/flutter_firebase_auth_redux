import 'package:firebase_auth/firebase_auth.dart';
import 'package:redux/redux.dart';

import '../authentication_actions.dart';

void deleteAccountMiddleware<T>(
    Store<T> store, DeleteAccountAction action, NextDispatcher next) async {
  try {
    FirebaseUser user = await FirebaseAuth.instance.currentUser();
    await user.delete();

    store.dispatch(new SignOutAction());
  } catch (e) {
    throw e;
  }
}
