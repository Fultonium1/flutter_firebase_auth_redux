import 'package:firebase_auth/firebase_auth.dart';
import 'package:redux/redux.dart';

import '../authentication_actions.dart';

void emailNotVerifiedMiddleware<T>(Store<T> store, EmailNotVerifiedAction action, NextDispatcher next) async {
  try {
    await FirebaseAuth.instance.signOut();
    next(action);
  } catch (e) {
    throw e;
  }
}