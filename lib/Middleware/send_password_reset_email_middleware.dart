import 'package:firebase_auth/firebase_auth.dart';
import 'package:redux/redux.dart';

import '../authentication_actions.dart';

void sendPasswordResetEmailMiddleware<T>(Store<T> store, SendPasswordResetEmailAction action, NextDispatcher next) {
  FirebaseAuth.instance.sendPasswordResetEmail(email: action.email)

      // Future.delayed(Duration(seconds: 2))
      .then((d) {
    if (action.onComplete != null) {
      action.onComplete();
    }
    store.dispatch(PasswordResetEmailSentAction(email: action.email));
  }).catchError((e) {
    throw e;
  });
}
