import 'package:firebase_auth/firebase_auth.dart';
import 'package:redux/redux.dart';

import '../authentication_actions.dart';

void sendVerificationEmailMiddleware<T>(Store<T> store,
    SendVerificationEmailAction action, NextDispatcher next) async {
  try {
    next(action);
    AuthResult result = await FirebaseAuth.instance.signInWithEmailAndPassword(
        email: action.email, password: action.password);
    FirebaseUser user = result.user;
    await user.sendEmailVerification();
    store.dispatch(VerificationEmailSentAction(
      email: action.email,
    ));
  } catch (e) {
    throw e;
  }
}
