import '../user/user.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:redux/redux.dart';

import '../authentication_actions.dart';

void signInWithEmailMiddleware<T>(
    Store<T> store, SignInWithEmailAction action, NextDispatcher next) async {
  try {
    FirebaseUser user = (await FirebaseAuth.instance.signInWithEmailAndPassword(
            email: action.email, password: action.password))
        .user;
    if (user?.isEmailVerified == true) {
      var token = await user.getIdToken(refresh: true);
      Map claims = token.claims;

      store.dispatch(SignedInAction(
        user: user.toUser().copyWith(claims: claims),
      ));
    } else {
      await FirebaseAuth.instance.signOut();
      store.dispatch(EmailNotVerifiedAction());
    }
  } catch (e) {
    String errorMessage = e.toString();
    if (errorMessage.contains("password is invalid")) {
      store.dispatch(InvalidPasswordAction());
    } else if (errorMessage.contains("no user record")) {
      store.dispatch(InvalidUserAction());
    } else {
      throw e;
    }
  }
}
