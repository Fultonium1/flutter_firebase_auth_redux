import 'package:firebase_auth/firebase_auth.dart';
import 'package:google_sign_in/google_sign_in.dart';
import 'package:redux/redux.dart';

import '../authentication_actions.dart';
import '../user/user.dart';

void signInWithGoogleMiddleware<T>(
    Store<T> store, SignInWithGoogleAction action, NextDispatcher next) async {
  try {
    GoogleSignIn _googleSignIn = new GoogleSignIn(
      scopes: [
        'email',
        'https://www.googleapis.com/auth/contacts.readonly',
      ],
    );
    GoogleSignInAccount gUser = await _googleSignIn.signIn();
    GoogleSignInAuthentication gAuth = await gUser.authentication;
    AuthCredential cred = GoogleAuthProvider.getCredential(
        accessToken: gAuth.accessToken, idToken: gAuth.idToken);
    FirebaseUser user =
        (await FirebaseAuth.instance.signInWithCredential(cred)).user;

    Map claims;
    if (user != null) {
      var token = await user.getIdToken(refresh: true);
      claims = token.claims;
    }

    store.dispatch(SignedInAction(
      user: user.toUser().copyWith(
            claims: claims,
          ),
    ));
  } catch (e) {
    throw e;
  }
}
