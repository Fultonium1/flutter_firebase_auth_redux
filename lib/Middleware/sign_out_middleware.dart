import 'package:firebase_auth/firebase_auth.dart';
import 'package:redux/redux.dart';

import '../authentication_actions.dart';

void signOutMiddleware<T>(
    Store<T> store, SignOutAction action, NextDispatcher next) async {
  try {
    await FirebaseAuth.instance.signOut();
  } catch (e) {
    throw e;
  } finally {
    store.dispatch(SignedOutAction());
  }
}

class ResetSessionStateAction {
}
