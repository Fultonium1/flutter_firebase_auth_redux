import 'package:firebase_auth/firebase_auth.dart';
import 'package:redux/redux.dart';

import '../authentication_actions.dart';

void signUpWithEmailMiddleware<T>(Store<T> store, SignUpWithEmailAction action, NextDispatcher next) async {
  try {
    next(action);
    FirebaseUser user = (await FirebaseAuth.instance.createUserWithEmailAndPassword(email: action.email, password: action.password)).user;
    UserUpdateInfo updateInfo = UserUpdateInfo();
    updateInfo.displayName = action.displayName;
    await user.updateProfile(updateInfo);

    if (user != null) {
      user.sendEmailVerification();
    }
    store.dispatch(VerificationEmailSentAction(email: action.email));
  } catch (e) {
    String errorMessage = e.toString();
    if (errorMessage.contains("already in use")) {
      store.dispatch(UserExistsAction());
    } else {
      throw e;
    }
  }
}