import 'package:redux/redux.dart';

import '../authentication_actions.dart';

void signedInMiddleware<T>(Store<T> store, SignedInAction action, NextDispatcher next) {
  next(action);
}