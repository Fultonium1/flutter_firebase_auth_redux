import 'package:redux/redux.dart';

import '../authentication_actions.dart';

void signedOutMiddleware<T>(Store<T> store, SignedOutAction action, NextDispatcher next) {
  next(action);
}
