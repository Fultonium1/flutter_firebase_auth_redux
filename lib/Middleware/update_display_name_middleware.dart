import 'package:firebase_auth/firebase_auth.dart';

import 'package:redux/redux.dart';

import '../authentication_actions.dart';

void updateDisplayNameMiddleware<T>(Store<T> store, UpdateNameAction action, NextDispatcher next) async
{
  UserUpdateInfo update = UserUpdateInfo();
  update.displayName = action.newName;
  await (await FirebaseAuth.instance.currentUser()).updateProfile(update);
  next(action);
}