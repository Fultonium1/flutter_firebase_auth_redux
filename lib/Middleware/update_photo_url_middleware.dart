import 'package:redux/redux.dart';

import '../authentication_actions.dart';

void updatePhotoUrlMiddleware<T>(
    Store<T> store, UpdatePhotoUrlAction action, NextDispatcher next) async {
  next(action);
}
