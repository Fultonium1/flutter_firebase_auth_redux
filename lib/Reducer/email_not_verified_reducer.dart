import '../authentication_actions.dart';
import '../authentication_state.dart';
import '../authentication_status.dart';

AuthenticationState emailNotVerifiedReducer(
    AuthenticationState state, EmailNotVerifiedAction action) {
  return state.copyWith(
      authenticationStatus: AuthenticationStatus.EmailNotVerified,
      user: () => state.currentUser?.copyWith(email: action.email));
}
