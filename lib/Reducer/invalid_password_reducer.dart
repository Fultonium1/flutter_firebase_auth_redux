import '../authentication_actions.dart';
import '../authentication_state.dart';
import '../authentication_status.dart';

AuthenticationState invalidPasswordReducer(
    AuthenticationState state, InvalidPasswordAction action) {
  return state.copyWith(
    authenticationStatus: AuthenticationStatus.InvalidPassword,
    user: () => null,
  );
}
