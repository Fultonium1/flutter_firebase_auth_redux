import '../authentication_actions.dart';
import '../authentication_state.dart';
import '../authentication_status.dart';

AuthenticationState invalidUserReducer(
    AuthenticationState state, InvalidUserAction action) {
  return state.copyWith(
    authenticationStatus: AuthenticationStatus.InvalidUser,
    user: () => null,
  );
}
