import '../authentication_actions.dart';
import '../authentication_state.dart';
import '../authentication_status.dart';
import '../user/user.dart';

AuthenticationState passwordResetEmailSentReducer(
    AuthenticationState state, PasswordResetEmailSentAction action) {
  return state.copyWith(
    authenticationStatus: AuthenticationStatus.PasswordResetSent,
    user: () => User(
      email: action.email,
    ),
  );
}
