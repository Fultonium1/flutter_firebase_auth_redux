import '../authentication_actions.dart';
import '../authentication_state.dart';
import '../authentication_status.dart';
import '../user/user.dart';

AuthenticationState signUpWithEmailReducer(
    AuthenticationState state, SignUpWithEmailAction action) {
  return state.copyWith(
      authenticationStatus: AuthenticationStatus.SendingVerificationEmail,
      user: () => User(
            email: action.email,
            name: action.displayName,
          ));
}
