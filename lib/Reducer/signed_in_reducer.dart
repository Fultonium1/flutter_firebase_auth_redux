import 'package:flutter_firebase_auth_redux/authentication_status.dart';

import '../authentication_actions.dart';
import '../authentication_state.dart';

AuthenticationState signedInReducer(
    AuthenticationState state, SignedInAction action) {
  return state.copyWith(
    authenticationStatus: AuthenticationStatus.SignedIn,
    user: () => action.user,
  );
}
