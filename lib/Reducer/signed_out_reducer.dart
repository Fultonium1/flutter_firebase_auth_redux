import '../authentication_actions.dart';
import '../authentication_state.dart';

AuthenticationState signedOutReducer(AuthenticationState state, SignedOutAction action)
{
  return AuthenticationState.initial();
}