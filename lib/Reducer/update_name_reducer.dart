import '../authentication_actions.dart';
import '../authentication_state.dart';

AuthenticationState updateNameReducer(
    AuthenticationState state, UpdateNameAction action) {
  return state.copyWith(
    user: () => state.currentUser.copyWith(name: action.newName),
  );
}
