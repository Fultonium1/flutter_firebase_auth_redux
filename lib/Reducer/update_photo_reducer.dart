import '../authentication_actions.dart';
import '../authentication_state.dart';

AuthenticationState updatePhotoUrlReducer(
    AuthenticationState state, UpdatePhotoUrlAction action) {
  return state.copyWith(
    user: () => state.currentUser.copyWith(
      photoUrl: action.newPhotoUrl,
    ),
  );
}
