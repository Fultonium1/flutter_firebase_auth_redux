import '../authentication_actions.dart';
import '../authentication_state.dart';
import '../authentication_status.dart';

AuthenticationState userExistsReducer(
    AuthenticationState state, UserExistsAction action) {
  return state.copyWith(
    authenticationStatus: AuthenticationStatus.UserExists,
  );
}
