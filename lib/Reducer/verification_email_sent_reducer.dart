import '../authentication_actions.dart';
import '../authentication_state.dart';
import '../authentication_status.dart';
import '../user/user.dart';

AuthenticationState verificationEmailSentReducer(
    AuthenticationState state, VerificationEmailSentAction action) {
  return state.copyWith(
      authenticationStatus: AuthenticationStatus.VerificationEmailSent,
      user: () => User(
            email: action.email,
          ));
}
