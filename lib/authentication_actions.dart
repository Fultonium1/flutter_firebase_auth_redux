import 'package:flutter/foundation.dart';

import 'user/user.dart';

class SignInWithGoogleAction {}

class SignInWithEmailAction {
  String email;
  String password;
  SignInWithEmailAction({this.email, this.password});
}

class SignOutAction {}

class SignUpWithEmailAction {
  String email;
  String password;
  String displayName;
  SignUpWithEmailAction({this.email, this.password, this.displayName});
}

class SendVerificationEmailAction {
  String email;
  String password;
  SendVerificationEmailAction({this.email, this.password});
}

class EmailNotVerifiedAction {
  String email;
}

class SendPasswordResetEmailAction {
  String email;
  VoidCallback onComplete;
  SendPasswordResetEmailAction({this.email, this.onComplete});
}

class SignedInAction {
  User user;

  SignedInAction({this.user});
}

class SignedOutAction {}

class VerificationEmailSentAction {
  String email;
  VerificationEmailSentAction({this.email});
}

class PasswordResetEmailSentAction {
  String email;

  PasswordResetEmailSentAction({this.email});
}

class CheckAuthenticatedAction {}

class InvalidPasswordAction {}

class InvalidUserAction {}

class UserExistsAction {}

class UpdatePhotoUrlAction {
  String newPhotoUrl;

  UpdatePhotoUrlAction({this.newPhotoUrl});
}

class UpdateNameAction {
  String newName;
  UpdateNameAction({this.newName});
}

class DeleteAccountAction {}
