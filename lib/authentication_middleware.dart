import 'package:redux/redux.dart';

import 'Middleware/check_authentication_middleware.dart';
import 'Middleware/delete_account_middleware.dart';
import 'Middleware/email_not_verified_middleware.dart';
import 'Middleware/send_password_reset_email_middleware.dart';
import 'Middleware/send_verification_email_middleware.dart';
import 'Middleware/sign_in_with_email_middleware.dart';
import 'Middleware/sign_in_with_google_middleware.dart';
import 'Middleware/sign_out_middleware.dart';
import 'Middleware/sign_up_with_email_middleware.dart';
import 'Middleware/signed_in_middleware.dart';
import 'Middleware/signed_out_middleware.dart';
import 'Middleware/update_display_name_middleware.dart';
import 'Middleware/update_photo_url_middleware.dart';
import 'authentication_actions.dart';
import 'authentication_reducer.dart';

List<Middleware<T>> authenticationMiddleware<T>() {
  return [
    TypedMiddleware<T, SignInWithGoogleAction>(signInWithGoogleMiddleware),
    TypedMiddleware<T, SignInWithEmailAction>(signInWithEmailMiddleware),
    TypedMiddleware<T, SignUpWithEmailAction>(signUpWithEmailMiddleware),
    TypedMiddleware<T, SendPasswordResetEmailAction>(sendPasswordResetEmailMiddleware),
    TypedMiddleware<T, SendVerificationEmailAction>(sendVerificationEmailMiddleware),
    TypedMiddleware<T, EmailNotVerifiedAction>(emailNotVerifiedMiddleware),
    TypedMiddleware<T, SignOutAction>(signOutMiddleware),
    TypedMiddleware<T, CheckAuthenticatedAction>(checkAuthenticatedMiddleware),
    TypedMiddleware<T, SignedInAction>(signedInMiddleware),
    TypedMiddleware<T, SignedOutAction>(signedOutMiddleware),
    TypedMiddleware<T, UpdateNameAction>(updateDisplayNameMiddleware),
    TypedMiddleware<T, UpdatePhotoUrlAction>(updatePhotoUrlMiddleware),
    TypedMiddleware<T, DeleteAccountAction>(deleteAccountMiddleware)
  ];
}