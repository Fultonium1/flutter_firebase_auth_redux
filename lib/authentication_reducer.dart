import 'package:redux/redux.dart';

import 'Reducer/email_not_verified_reducer.dart';
import 'Reducer/invalid_password_reducer.dart';
import 'Reducer/invalid_user_reducer.dart';
import 'Reducer/password_email_resent_reducer.dart';
import 'Reducer/sign_up_with_email_reducer.dart';
import 'Reducer/signed_in_reducer.dart';
import 'Reducer/signed_out_reducer.dart';
import 'Reducer/update_name_reducer.dart';
import 'Reducer/update_photo_reducer.dart';
import 'Reducer/user_exists_reducer.dart';
import 'Reducer/verification_email_sent_reducer.dart';
import 'authentication_actions.dart';
import 'authentication_state.dart';

final authenticationReducers = combineReducers<AuthenticationState>(
[
    TypedReducer<AuthenticationState, SignedInAction>(signedInReducer),
    TypedReducer<AuthenticationState, SignedOutAction>(signedOutReducer),
    TypedReducer<AuthenticationState, VerificationEmailSentAction>(verificationEmailSentReducer),
    TypedReducer<AuthenticationState, PasswordResetEmailSentAction>(passwordResetEmailSentReducer),
    TypedReducer<AuthenticationState, EmailNotVerifiedAction>(emailNotVerifiedReducer),
    TypedReducer<AuthenticationState, InvalidPasswordAction>(invalidPasswordReducer),
    TypedReducer<AuthenticationState, InvalidUserAction>(invalidUserReducer),
    TypedReducer<AuthenticationState, UserExistsAction>(userExistsReducer),
    TypedReducer<AuthenticationState, UpdateNameAction>(updateNameReducer),
    TypedReducer<AuthenticationState, UpdatePhotoUrlAction>(updatePhotoUrlReducer),
    TypedReducer<AuthenticationState, SignUpWithEmailAction>(signUpWithEmailReducer),
  ]);