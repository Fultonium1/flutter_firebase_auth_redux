import 'package:flutter/foundation.dart';
import 'package:flutter_firebase_auth_redux/authentication_status.dart';
import 'authentication_status.dart';
import 'user/user.dart';
import 'package:json_annotation/json_annotation.dart';

part 'authentication_state.g.dart';

@JsonSerializable(explicitToJson: true, anyMap: true, checked: true)
@immutable
class AuthenticationState {
  final User currentUser;
  final AuthenticationStatus authenticationStatus;

  AuthenticationState({this.currentUser, this.authenticationStatus});

  AuthenticationState copyWith(
      {ValueGetter<User> user, AuthenticationStatus authenticationStatus}) {
    return AuthenticationState(
      currentUser: user != null ? user() : this.currentUser,
      authenticationStatus: authenticationStatus ?? this.authenticationStatus,
    );
  }

  factory AuthenticationState.initial() {
    return AuthenticationState(
      currentUser: null,
      authenticationStatus: AuthenticationStatus.SignedOut,
    );
  }

  AuthenticationState validate() {
    return AuthenticationState(
      authenticationStatus:
          authenticationStatus ?? AuthenticationStatus.SignedOut,
      currentUser: currentUser,
    );
  }

  factory AuthenticationState.fromJson(Map<String, dynamic> json) =>
      _$AuthenticationStateFromJson(json);
  Map<String, dynamic> toJson() => _$AuthenticationStateToJson(this);
}
