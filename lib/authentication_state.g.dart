// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'authentication_state.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

AuthenticationState _$AuthenticationStateFromJson(Map json) {
  return $checkedNew('AuthenticationState', json, () {
    final val = AuthenticationState(
      currentUser: $checkedConvert(
          json,
          'currentUser',
          (v) => v == null
              ? null
              : User.fromJson((v as Map)?.map(
                  (k, e) => MapEntry(k as String, e),
                ))),
      authenticationStatus: $checkedConvert(json, 'authenticationStatus',
          (v) => _$enumDecodeNullable(_$AuthenticationStatusEnumMap, v)),
    );
    return val;
  });
}

Map<String, dynamic> _$AuthenticationStateToJson(
        AuthenticationState instance) =>
    <String, dynamic>{
      'currentUser': instance.currentUser?.toJson(),
      'authenticationStatus':
          _$AuthenticationStatusEnumMap[instance.authenticationStatus],
    };

T _$enumDecode<T>(
  Map<T, dynamic> enumValues,
  dynamic source, {
  T unknownValue,
}) {
  if (source == null) {
    throw ArgumentError('A value must be provided. Supported values: '
        '${enumValues.values.join(', ')}');
  }

  final value = enumValues.entries
      .singleWhere((e) => e.value == source, orElse: () => null)
      ?.key;

  if (value == null && unknownValue == null) {
    throw ArgumentError('`$source` is not one of the supported values: '
        '${enumValues.values.join(', ')}');
  }
  return value ?? unknownValue;
}

T _$enumDecodeNullable<T>(
  Map<T, dynamic> enumValues,
  dynamic source, {
  T unknownValue,
}) {
  if (source == null) {
    return null;
  }
  return _$enumDecode<T>(enumValues, source, unknownValue: unknownValue);
}

const _$AuthenticationStatusEnumMap = {
  AuthenticationStatus.InvalidUser: 'InvalidUser',
  AuthenticationStatus.UserExists: 'UserExists',
  AuthenticationStatus.SendingVerificationEmail: 'SendingVerificationEmail',
  AuthenticationStatus.VerificationEmailSent: 'VerificationEmailSent',
  AuthenticationStatus.EmailNotVerified: 'EmailNotVerified',
  AuthenticationStatus.PasswordResetSent: 'PasswordResetSent',
  AuthenticationStatus.InvalidPassword: 'InvalidPassword',
  AuthenticationStatus.SignedIn: 'SignedIn',
  AuthenticationStatus.SignedOut: 'SignedOut',
};
