enum AuthenticationStatus
{
  InvalidUser,
  UserExists,
  SendingVerificationEmail,
  VerificationEmailSent,
  EmailNotVerified,
  PasswordResetSent,
  InvalidPassword,
  SignedIn,
  SignedOut
}