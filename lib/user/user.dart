import 'package:flutter/foundation.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:json_annotation/json_annotation.dart';

part 'user.g.dart';

@immutable
@JsonSerializable(explicitToJson: true, anyMap: true, checked: true)
class User {
  final String id;
  final String name;
  final String email;
  final bool emailVerified;
  final String phoneNumber;
  final String photoUrl;
  final Map<dynamic, dynamic> claims;

  User({
    this.id,
    this.name,
    this.email,
    this.emailVerified,
    this.phoneNumber,
    this.photoUrl,
    this.claims,
  });

  User copyWith({
    String id,
    String name,
    String email,
    bool emailVerified,
    String phoneNumber,
    String photoUrl,
    Map<dynamic, dynamic> claims,
  }) {
    return User(
      id: id ?? this.id,
      email: email ?? this.email,
      emailVerified: emailVerified ?? this.emailVerified,
      name: name ?? this.name,
      phoneNumber: phoneNumber ?? this.phoneNumber,
      photoUrl: photoUrl ?? this.photoUrl,
      claims: claims ?? this.claims,
    );
  }

  factory User.fromJson(Map<String, dynamic> json) => _$UserFromJson(json);
  Map<String, dynamic> toJson() => _$UserToJson(this);
}

extension FirebaseUserExtensions on FirebaseUser {
  User toUser() {
    return User(
      id: this.uid,
      email: this.email,
      emailVerified: this.isEmailVerified,
      name: this.displayName,
      phoneNumber: this.phoneNumber,
    );
  }
}
