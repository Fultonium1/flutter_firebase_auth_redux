// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'user.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

User _$UserFromJson(Map json) {
  return $checkedNew('User', json, () {
    final val = User(
      id: $checkedConvert(json, 'id', (v) => v as String),
      name: $checkedConvert(json, 'name', (v) => v as String),
      email: $checkedConvert(json, 'email', (v) => v as String),
      emailVerified: $checkedConvert(json, 'emailVerified', (v) => v as bool),
      phoneNumber: $checkedConvert(json, 'phoneNumber', (v) => v as String),
      photoUrl: $checkedConvert(json, 'photoUrl', (v) => v as String),
      claims: $checkedConvert(json, 'claims', (v) => v as Map),
    );
    return val;
  });
}

Map<String, dynamic> _$UserToJson(User instance) => <String, dynamic>{
      'id': instance.id,
      'name': instance.name,
      'email': instance.email,
      'emailVerified': instance.emailVerified,
      'phoneNumber': instance.phoneNumber,
      'photoUrl': instance.photoUrl,
      'claims': instance.claims,
    };
